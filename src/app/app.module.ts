import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';

import { FilmComponent } from './film/film.component';
import {RouterModule} from "@angular/router";
import {HttpClientModule} from "@angular/common/http";
import {ReactiveFormsModule} from "@angular/forms";
import { FilmDetailsComponent } from './film-details/film-details.component';
import { FilmListComponent } from './film-list/film-list.component';
import { FilmFormComponent } from './film-form/film-form.component';
import { FilmUpdateComponent } from './film-update/film-update.component';

@NgModule({
  declarations: [
    AppComponent,
    FilmComponent,
    FilmDetailsComponent,
    FilmListComponent,
    FilmFormComponent,
    FilmUpdateComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,


    //pour configurer des routes
    RouterModule.forRoot([
      {path: 'films/new', component: FilmFormComponent},
      {path: 'films/:id', component: FilmDetailsComponent },
      {path: '', component: FilmListComponent},
      {path: 'films/update/:id', component: FilmUpdateComponent},

    ]),




  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
