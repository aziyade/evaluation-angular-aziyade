import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Film} from "./film.component";
import {Observable} from "rxjs";


@Injectable({
  providedIn: 'root'
})
export class FilmService {

  constructor(private http: HttpClient) {

  }


  getAllFilms() {
    return this.http.get<Film[]>('http://localhost:3000/films');
  }

  getFilmById(id: number){
    return this.http.get<Film>(`http://localhost:3000/films/${id}`);

  }

  addFilm(film: Film){
    return this.http.post('http://localhost:3000/films', film);

  }

  updateFilm(film: Film): Observable<Film> {
    const url = `http://localhost:3000/films/${film.id}`;
    return this.http.put<Film>(url, film);
  }

  deleteFilm(id:number): Observable<unknown> {
    const url = `http://localhost:3000/films/${id}`;
      return this.http.delete(url);
    }



}
