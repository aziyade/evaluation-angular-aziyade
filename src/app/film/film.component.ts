import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from "rxjs";
import {FilmService} from "./film.service";


export interface Film {
  id: number;
  title: string;
  synopsis:string;
  releaseYear: number;
  director: string;
  imbdRating: number;

}



@Component({
  selector: 'app-film',
  templateUrl: './film.component.html',
  styleUrls: ['./film.component.css']
})
export class FilmComponent implements OnInit {

  constructor(private http: HttpClient, private filmService: FilmService) { }


  films$!: Observable<Film[]>


  ngOnInit(): void {
    this.films$ = this.filmService.getAllFilms();
  }

  delete(id: number): void {
    this.filmService.deleteFilm(id).subscribe(() => {
      this.films$ = this.filmService.getAllFilms();

    })
  }
}
