import { Component, OnInit } from '@angular/core';
import {FilmService} from "../film/film.service";
import {FormBuilder, Validators} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {Film} from "../film/film.component";



@Component({
  selector: 'app-film-form',
  templateUrl: './film-form.component.html',
  styleUrls: ['./film-form.component.css']
})
export class FilmFormComponent implements OnInit {

  filmForm = this.formBuilder.group({
    title: ['', Validators.required],
    synopsis: [''],
    releaseYear: [''],
    director: [''],
    imbdRating: [''],


  })


  constructor(private formBuilder: FormBuilder, private http: HttpClient, private filmService: FilmService) { }

  ngOnInit(): void {
  }


  onSubmit() {

    let film:Film = this.filmForm.value;
    this.filmService.addFilm(film).subscribe();

    console.warn(this.filmForm.value);
  }

  updateFilm() {
    this.filmForm.patchValue({
      title: 'starwars',
      synopsis: '...',
      releaseYear: 2000,
      director: 'lucas',
      imbdRating: 9,
    });
  }

}
