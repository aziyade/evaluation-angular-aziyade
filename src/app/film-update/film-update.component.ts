import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {Film} from "../film/film.component";
import {FormBuilder, Validators} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {FilmService} from "../film/film.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-film-update',
  templateUrl: './film-update.component.html',
  styleUrls: ['./film-update.component.css']
})
export class FilmUpdateComponent implements OnInit {

  film$: Observable<Film> | undefined;


  filmUpdate = this.formBuilder.group({
    title: ['', Validators.required],
    synopsis: [''],
    releaseYear: [''],
    director: [''],
    imbdRating: [''],

  })

  constructor(private formBuilder: FormBuilder, private http: HttpClient,
              private filmService: FilmService, private  route: ActivatedRoute) { }

  ngOnInit(): void {
    const routeParam = this.route.snapshot.paramMap;
    const filmId = Number(routeParam.get("id"));
    this.film$ = this.filmService.getFilmById(filmId);
    this.film$.subscribe( p => {
      console.log(p);
      this.filmUpdate.patchValue(p)
    })
  }


  updateFilm() {
    let film:Film = this.filmUpdate.value;
    film.id = Number(this.route.snapshot.paramMap.get("id"));
    this.filmService.updateFilm(film).subscribe();

    console.warn(this.filmUpdate.value);
  }
}
