import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {Film} from "../film/film.component";
import {FilmService} from "../film/film.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-film-details',
  templateUrl: './film-details.component.html',
  styleUrls: ['./film-details.component.css']
})
export class FilmDetailsComponent implements OnInit {
  films$!: Observable<Film>;

  constructor(private filmService: FilmService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    const routeParam = this.route.snapshot.paramMap;
    const filmId = Number(routeParam.get("id"));
    this.films$ = this.filmService.getFilmById(filmId);
    this.films$.subscribe(p => console.log(p));

  }

}
