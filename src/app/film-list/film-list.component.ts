import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {Film} from "../film/film.component";
import {ActivatedRoute} from "@angular/router";
import {FilmService} from "../film/film.service";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-film-list',
  templateUrl: './film-list.component.html',
  styleUrls: ['./film-list.component.css']
})
export class FilmListComponent implements OnInit {
  films$!: Observable<Film[]>;



  constructor(private filmService: FilmService,
              private route: ActivatedRoute, private http: HttpClient) { }


  ngOnInit(): void {

    this.films$ = this.filmService.getAllFilms();

  }

  deleteFilm(id:number): void {
    const url = `http://localhost:3000/films/${id}`;
    this.http.delete(url).subscribe(p => console.log(p));
    window.location.reload();
  }

}
